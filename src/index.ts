import './style.css'

interface IButton {
    id: string
    icon: string
    audio: string
    background: string
}

const buttonsFeatures: IButton[] = [
    {
        id: 'button1',
        icon: 'assets/icons/sun.svg',
        audio: 'assets/sounds/summer.mp3',
        background: "url('assets/summer-bg.jpg')"
    },
    {
        id: 'button2',
        icon: 'assets/icons/cloud-rain.svg',
        audio: 'assets/sounds/rain.mp3',
        background: "url('assets/rainy-bg.jpg')"
    },
    {
        id: 'button3',
        icon: 'assets/icons/cloud-snow.svg',
        audio: 'assets/sounds/winter.mp3',
        background: "url('assets/winter-bg.jpg')"
    }
]
const buttons: HTMLButtonElement[] = buttonsFeatures.map(button => document.getElementById(button.id) as HTMLButtonElement)
const audios: HTMLAudioElement[] = buttonsFeatures.map(button => new Audio(button.audio) as HTMLAudioElement)

const volumeInput = document.getElementById("volume") as HTMLInputElement

volumeInput.addEventListener("input", () => {
  const volume = Number(volumeInput.value)

  if (currentAudioId !== null) {
    audios[currentAudioId].volume = volume
  }
})

buttons.forEach(button => {
    button.style.backgroundImage = buttonsFeatures[buttons.findIndex(item => item.id === button.id)].background
    button.addEventListener("click", () => {
        onButtonClick(button)
    })
})

let currentAudioId: number | null = null

function onButtonClick(button: HTMLButtonElement): void {
    const id: number = buttons.findIndex(item => item.id === button.id)
    const audio: HTMLAudioElement = audios[id]
    if (currentAudioId !== null) {
        audios[currentAudioId].pause()
        changeIcon(currentAudioId)
    }
    if (currentAudioId === null || audio !== audios[currentAudioId]) {
        audio.volume = Number(volumeInput.value)
        audio.play()
        currentAudioId = id
        changeIcon(id, true)
        changeBackground(id)
    } else {
        audio.pause()
        currentAudioId = null
        changeIcon(id)
    }
}

function changeIcon(id: number, showPause: boolean = false): void {
    const icon: HTMLImageElement | null = document.querySelector(
        `#${buttons[id].id} img`
    )
    if (icon !== null) {
        icon.src = showPause ? 'assets/icons/pause.svg' : buttonsFeatures[id].icon
    }
}

function changeBackground(id: number): void {
    document.body.style.backgroundImage = buttonsFeatures[id].background
}
